#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from PyQt5 import QtWidgets

__author__ = "d41"

from modules import blend


def main():
    app = QtWidgets.QApplication(sys.argv)
    widget = blend.ProjectorBlend()
    widget.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
