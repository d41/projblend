# Calculator for projection blending

Calculate required projectors amount to completely fill projection surface. 
Try to find optimal variant based on projectors aspect ratio.

## First step in implementation

* Rectangular horizontal projection surface
* All projectors have same characteristics 
* Projector fill complete height (no vertical masking)
* Actual overlap calculation based on desired minimum (can be bigger, but can't be smaller)
* Variants of different aspect ratio projector shown sorted (based on optimal blend percentage)
* Draw sample installation schemes
* Save to file (via print preview dialog with save to pdf option)


## TODO
* Save to editable file
* Template images generation for actual projectors setup
* Refactor UI to make place for template generation, report text and other things

## Screenshots

First version:

![Screenshot #0](https://bitbucket.org/d41/projblend/downloads/ProjBlend-screen-0.png "Screenshot #0")

