# compile designer's UI file

IF_DIR := interfaces
MO_DIR := modules

NAMES := \
	blend

INTERFACES := $(addprefix $(IF_DIR)/, $(addsuffix .ui,$(NAMES)))
MODULES := $(addprefix $(MO_DIR)/, $(addsuffix _ui.py,$(NAMES)))
SOURCES := $(addprefix $(MO_DIR)/, $(addsuffix .py,$(NAMES)))

all: $(MODULES)

$(MO_DIR)/%_ui.py: $(IF_DIR)/%.ui
	python3 -m PyQt5.uic.pyuic -o $@ $<

run: 
	python3 ./proj_blend.py

clean:
	rm $(MO_DIR)/*.pyc
	cd $(MO_DIR) && rm __pycache__ -rf

#$(IF_DIR)/projection_ru.ts: $(INTERFACES)
#	lupdate -qt=qt5 -source-language en -target-language ru $(IF_DIR)/*.ui -ts $@ -verbose
