# -*- coding: utf-8 -*-

import logging
import math
from collections import namedtuple

from PyQt5 import QtWidgets, QtCore, QtGui

from .blend_ui import Ui_ProjectorBlend
from .scheme import FrontScheme, TopScheme
from .calculations import get_real_blend

__author__ = "d41"
# TODO: научиться делать отчет в "печатном" варианте, короче чтобы вываливать PDF или RTF
# TODO: подшаманить интерфейс для того чтобы там был экспорт, печать и генерация шаблонов по параметрам, а не как сейчас
# TODO: после этого можно генерировать шаблоны настройки и проверки сшивки


logger = logging.getLogger("blend")
logger.setLevel(logging.DEBUG)

ProjectionInfo = namedtuple("ProjectionInfo", ("width", "height", "res_x", "res_y"))


class ProjectorBlend(QtWidgets.QWidget):
    def __init__(self):
        super(ProjectorBlend, self).__init__()

        self.ui = Ui_ProjectorBlend()
        self.ui.setupUi(self)

        self.setup_headers()

        self.frontScheme = FrontScheme(self.ui.gvFront)
        self.ui.gvFront.setScene(self.frontScheme)

        self.topScheme = TopScheme(self.ui.gvTop)
        self.ui.gvTop.setScene(self.topScheme)

        self.data_for_export = None

        self.ui.variants.itemSelectionChanged.connect(self.set_resolution_list)

    def setup_headers(self):
        """
        Fill header names in table.
        """
        header = self.ui.variants.horizontalHeader()
        # Set headers
        self.ui.variants.setHorizontalHeaderLabels([
            header.tr("Aspect"), header.tr("Blend, %"), header.tr("Blend, mm"), header.tr("Projectors")
        ])

    @QtCore.pyqtSlot()
    def on_calc_clicked(self):
        """
        Click on Calculate button processed here.

        Two steps:
        1. Calculate projectors for required minimal blend percentage (three aspect ratios).
        2. Calculate percentage for actual amount of projectors (three aspect ratios).

        And then fill the table sorted by percentage.
        """

        # clear table, to be able to check that calculation started, but failed
        self.ui.variants.clearContents()

        width = self.ui.surfaceWidth.value()
        height = self.ui.surfaceHeight.value()
        blend = self.ui.blendMinimal.value()
        logger.info("data: w={}, h={}, b={}".format(width, height, blend))
        self.clearFormsData()

        if width < height:
            logger.error("not supported layout in this version")
            return

        # check for real blend for every aspect 4:3, 16:10, 16:9 and count projectors
        variants = []
        for aspect in [12, 10, 9]:
            variants.append(get_real_blend(width, height, aspect, blend))

        variants.sort(key=lambda x: x[1])

        for num, var in enumerate(variants):
            # Round some values, simplify aspect
            test = math.gcd(16, var[0])
            if test == 4:
                text_aspect = "{:d}:{:d}".format(int(16 / test), int(var[0] / test))
            else:
                text_aspect = "16:{}".format(var[0])
            cell_aspect = QtWidgets.QTableWidgetItem(text_aspect)
            cell_aspect.setData(QtCore.Qt.UserRole, var[0])

            cell_bpercent = QtWidgets.QTableWidgetItem("{:.01f}".format(var[1]))
            cell_bpercent.setData(QtCore.Qt.UserRole, var[1])

            cell_bwidth = QtWidgets.QTableWidgetItem("{:.01f}".format(var[2]))

            cell_proj_count = QtWidgets.QTableWidgetItem("{}".format(var[3]))
            cell_proj_count.setData(QtCore.Qt.UserRole, var[3])

            self.ui.variants.setItem(num, 0, cell_aspect)
            self.ui.variants.setItem(num, 1, cell_bpercent)
            self.ui.variants.setItem(num, 2, cell_bwidth)
            self.ui.variants.setItem(num, 3, cell_proj_count)

    def set_resolution_list(self):
        """
        Fill combo box with resolutions matching selected aspect ratio
        """
        resolutions = {
            9: [(1920, 1080), (1280, 720)],
            10: [(1920, 1200), (1280, 800)],
            12: [(1400, 1050), (1024, 768)],
        }

        self.ui.resolutions.clear()
        items = self.ui.variants.selectedItems()

        found_items = len(items) != 0
        if found_items:
            aspect = items[0].data(QtCore.Qt.UserRole)
            if aspect in resolutions:
                for x, y in resolutions[aspect]:
                    self.ui.resolutions.addItem("{} x {}".format(x, y), userData=(x, y))
            else:
                logger.error("Wrong selected aspect ratio!")

        self.ui.resolutions.setEnabled(found_items)
        self.ui.drawSchemes.setEnabled(found_items)
        self.clearFormsData()

    @QtCore.pyqtSlot()
    def on_drawSchemes_clicked(self):
        """
        Calculate surface size and edge blending in pixels, and projectors locations

        :param e:
        :return:
        """

        # get main info
        surface_w, surface_h = self.ui.surfaceWidth.value(), self.ui.surfaceHeight.value()
        projector_x, projector_y = self.ui.resolutions.currentData(QtCore.Qt.UserRole)

        # gathering additional info
        items = self.ui.variants.selectedItems()
        found_items = len(items) != 0
        if found_items:
            aspect = items[0].data(QtCore.Qt.UserRole)
            blend = items[1].data(QtCore.Qt.UserRole)
            projector_count = items[3].data(QtCore.Qt.UserRole)
        throw_ratio = self.ui.throwRatio.value()

        # scale down dimensions to CM before drawing
        scale = 10.0  # mm to cm

        # TODO: потом вынести это всё в вычисления
        # gather single projection info
        projector_w, projector_h = (surface_h / aspect) * 16, surface_h
        projection = ProjectionInfo(projector_w / scale, projector_h / scale, projector_x, projector_y)

        # gather surface info
        blend_x = projector_x * (blend / 100.0)

        if projector_count > 1:
            surface_x = int((projector_x - blend_x) * projector_count + blend_x)
        else:
            surface_x = int(projector_y / surface_h * surface_w)

        surface_y = projector_y
        surface = ProjectionInfo(surface_w / scale, surface_h / scale, surface_x, surface_y)
        self.ui.exportData.setEnabled(True)

        # additional info
        blend_w = projector_w * (blend / 100.0)
        distance = (projector_w * throw_ratio) / scale
        side_step = (projector_w / 2) / scale
        main_step = (projector_w - blend_w) / scale

        # draw schemes
        self.frontScheme.initializeItems(surface, projector_count, projection, side_step, main_step)
        self.topScheme.initializeItems(surface, projector_count, side_step, main_step, distance)

        # fill form values
        self.ui.surfaceResolution.setText("{} x {}".format(surface.res_x, surface.res_y))
        self.ui.projectionSize.setText("{:0.0f} x {:0.0f}".format(projector_w, projector_h))
        self.ui.pixelSize.setText("{:0.1f}".format(surface_h / surface_y))
        self.data_for_export = (aspect, blend, blend_w, blend_x, projector_count, self.ui.resolutions.currentText())

    def clearFormsData(self):
        self.ui.surfaceResolution.setText("")
        self.ui.projectionSize.setText("")
        self.ui.pixelSize.setText("")
        self.ui.exportData.setEnabled(False)

    @QtCore.pyqtSlot(int)
    def on_scaleFactor_valueChanged(self, value):
        """
        Scale schematics of projection surface

        :param value: scale factor, 5 = 0,5x, 10 = 1x, 20=2x
        :return:
        """
        self.scale = value / 10.0
        self.ui.lScale.setText("{:0.1f}x".format(self.scale))

        self.ui.gvFront.resetTransform()
        self.ui.gvTop.resetTransform()
        self.ui.gvFront.scale(self.scale, self.scale)
        self.ui.gvTop.scale(self.scale, self.scale)

    @QtCore.pyqtSlot()
    def on_exportData_clicked(self):
        """
        Export results to file. Right now it is printing. (Print to file option).

        :return: nothing
        """
        editor = QtWidgets.QTextEdit()
        document = QtGui.QTextDocument()
        editor.setDocument(document)

        scene_w, scene_h = self.ui.gvFront.scene().width(), self.ui.gvFront.scene().height()
        image = QtGui.QImage(scene_w, scene_h, QtGui.QImage.Format_ARGB32_Premultiplied)
        image.fill(0)
        painter = QtGui.QPainter(image)
        self.ui.gvFront.scene().render(painter)
        painter.end()

        # image.save("front.png")  # this is test
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/front.png"), image)

        scene_w, scene_h = self.ui.gvTop.scene().width(), self.ui.gvTop.scene().height()
        image = QtGui.QImage(scene_w, scene_h, QtGui.QImage.Format_ARGB32_Premultiplied)
        image.fill(0)
        painter = QtGui.QPainter(image)
        self.ui.gvTop.scene().render(painter)
        painter.end()

        # image.save("top.png")  # this is test
        document.addResource(QtGui.QTextDocument.ImageResource, QtCore.QUrl("dynamic:/images/top.png"), image)

        # TODO: prepare export document (print preview just for visual test)
        # prepare and process actual data
        export_data = {
            "surface_id": self.ui.surfaceID.text(),
            "notes": self.ui.notes.toPlainText().replace("\n", "<br/>"),
            "surface_size": "{} x {}".format(self.ui.surfaceWidth.value(), self.ui.surfaceHeight.value()),
            "surface_resolution": self.ui.surfaceResolution.text(),
            "projectors_count": "{}".format(self.data_for_export[4]),
            "blend_mm": "{:0.0f}".format(self.data_for_export[2]),
            "blend_px": "{:0.0f}".format(self.data_for_export[3]),
            "projector_size": self.ui.projectionSize.text(),
            "projector_resolution": "{}".format(self.data_for_export[5]),
            "throw_ratio": self.ui.throwRatio.text(),
            "pixel_size": self.ui.pixelSize.text().replace('.', ',')
        }
        print(export_data)
        with open("templates/report.html", "r") as f:
            template = f.read()
            text = template.format(**export_data)
            editor.append(text)
            document.end()

        from PyQt5 import QtPrintSupport
        printer = QtPrintSupport.QPrinter()
        print_dialog = QtPrintSupport.QPrintPreviewDialog(printer)
        print_dialog.paintRequested.connect(editor.print)
        if print_dialog.exec() != print_dialog.Accepted:
            return

        document.print(printer)
