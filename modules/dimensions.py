# -*- coding: utf-8 -*-

import math
import logging

from PyQt5 import QtWidgets, QtGui, QtCore

__author__ = "d41"

logger = logging.getLogger("blend")
logger.setLevel(logging.DEBUG)


class HorizontalDimensionItem(QtWidgets.QGraphicsItem):
    def __init__(self, x1, y_base, x2, y_shift, label="", parent=None):
        super().__init__(parent)

        self.pen_dimensions = QtGui.QPen(QtCore.Qt.black)
        self.pen_dimensions.setWidth(1)
        self.font = QtGui.QFont("Serif", 12)

        self.x1, self.x2 = x1, x2
        self.y_base = y_base
        self.y_shift = y_shift
        self.label = label
        self.font_height = QtGui.QFontMetricsF(self.font).height()

        direction = +1 if self.y_shift > self.y_base else -1
        self.tail = direction * 4
        self.arrow_x = 8
        self.arrow_y = 2

    def boundingRect(self):
        # recalculate using actual data and label
        shift = self.tail
        if shift < 0:
            shift = -self.font_height

        left = self.x1
        top = self.y_base if self.tail > 0 else self.y_shift + shift
        width = math.fabs(self.x2 - self.x1)
        height = math.fabs(self.y_shift - self.y_base + shift)
        return QtCore.QRectF(left, top, width, height)

    def paint(self, painter, option, widget):
        painter.setPen(self.pen_dimensions)
        painter.setFont(self.font)

        # draw link lines
        painter.drawLine(self.x1, self.y_base, self.x1, self.y_shift + self.tail)
        painter.drawLine(self.x2, self.y_base, self.x2, self.y_shift + self.tail)

        # draw main line
        painter.drawLine(self.x1, self.y_shift, self.x2, self.y_shift)

        # draw arrows
        painter.drawLine(self.x1, self.y_shift, self.x1 + self.arrow_x, self.y_shift + self.arrow_y)
        painter.drawLine(self.x1, self.y_shift, self.x1 + self.arrow_x, self.y_shift - self.arrow_y)
        painter.drawLine(self.x2, self.y_shift, self.x2 - self.arrow_x, self.y_shift + self.arrow_y)
        painter.drawLine(self.x2, self.y_shift, self.x2 - self.arrow_x, self.y_shift - self.arrow_y)

        # draw label
        painter.drawText(QtCore.QRectF(self.x1, self.y_shift - self.font_height, self.x2 - self.x1, self.font_height),
                         QtCore.Qt.AlignCenter, self.label)

        # painter.setPen(QtGui.QPen(QtCore.Qt.blue))
        # painter.drawRect(self.boundingRect())


class VerticalDimensionItem(QtWidgets.QGraphicsItem):
    def __init__(self, x_base, y1, x_shift, y2, label="", parent=None):
        super().__init__(parent)

        self.pen_dimensions = QtGui.QPen(QtCore.Qt.black)
        self.pen_dimensions.setWidth(1)
        self.font = QtGui.QFont("Serif", 12)

        self.x_base, self.x_shift = x_base, x_shift
        self.y1, self.y2 = y1, y2
        self.label = label
        self.font_height = QtGui.QFontMetricsF(self.font).height()

        direction = +1 if self.x_shift > self.x_base else -1
        self.tail = direction * 5
        self.arrow_y = 8
        self.arrow_x = 3

    def boundingRect(self):
        # recalculate using actual data and label
        shift = self.tail
        if shift < 0:
            shift = -self.font_height

        left = min(self.x_base, self.x_shift + shift)
        top = min(self.y1, self.y2)
        width = math.fabs(self.x_shift - self.x_base + shift)
        height = math.fabs(self.y2 - self.y1)

        return QtCore.QRectF(left, top, width, height)

    def paint(self, painter, option, widget):
        painter.setPen(self.pen_dimensions)
        painter.setFont(self.font)

        # draw link lines
        painter.drawLine(self.x_base, self.y1, self.x_shift + self.tail, self.y1)
        painter.drawLine(self.x_base, self.y2, self.x_shift + self.tail, self.y2)

        # draw main line
        painter.drawLine(self.x_shift, self.y1, self.x_shift, self.y2)

        # draw arrows
        painter.drawLine(self.x_shift, self.y1, self.x_shift + self.arrow_x, self.y1 + self.arrow_y)
        painter.drawLine(self.x_shift, self.y1, self.x_shift - self.arrow_x, self.y1 + self.arrow_y)
        painter.drawLine(self.x_shift, self.y2, self.x_shift + self.arrow_x, self.y2 - self.arrow_y)
        painter.drawLine(self.x_shift, self.y2, self.x_shift - self.arrow_x, self.y2 - self.arrow_y)

        # draw label
        down = max(self.y1, self.y2)
        left = min(self.x_shift, self.x_base)
        width = math.fabs(self.y2 - self.y1)
        painter.save()
        painter.translate(left, down)
        painter.rotate(-90)
        painter.drawText(QtCore.QRectF(0, -self.font_height, width, self.font_height),
                         QtCore.Qt.AlignHCenter, self.label)
        painter.restore()

        # painter.setPen(QtGui.QPen(QtCore.Qt.green))
        # painter.drawRect(self.boundingRect())
