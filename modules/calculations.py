# -*- coding: utf-8 -*-

import logging
import math

__author__ = "d41"

logger = logging.getLogger("blend")
logger.setLevel(logging.DEBUG)


def get_real_blend(width, height, aspect, min_blend):
    """
    Calculate projection system for selected surface and with projector of desired aspect ratio.

    :param width: width of projection surface
    :param height: height of projection surface
    :param aspect: vertical part of aspect ratio (with horizontal based on 16, i.e. 4:3 is 12)
    :param min_blend: minimal blend width in percents
    :return: current aspect, blend in percents, blend in mm, and projectors count
    """
    screen_width = 16 * (height / aspect)
    blend_width = screen_width * (min_blend / 100)
    logger.info("16:{} w={}, h={}, b={}".format(aspect, screen_width, height, blend_width))

    screens_count = (width - blend_width) / (screen_width - blend_width)
    projectors_count = math.ceil(screens_count)

    # used formula calculated this way: (x is a real blend width)
    # (width-x)/(screen_width-x) = projectors_count
    # width-x = projectors_count*screen_width - projectors_count*x
    # width - projectors_count*screen_width = x - projectors_count*x
    if projectors_count == 1:
        # to avoid division on zero
        new_blend = screen_width
        blend_percent = 100
    else:
        new_blend = (width - projectors_count * screen_width) / (1 - projectors_count)
        blend_percent = (new_blend / screen_width) * 100

    logger.info("projectors={}, b={}, b%={}".format(projectors_count, new_blend, blend_percent))
    return aspect, blend_percent, new_blend, projectors_count
