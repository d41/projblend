# -*- coding: utf-8 -*-

import logging

from PyQt5 import QtWidgets, QtGui, QtCore

from .dimensions import HorizontalDimensionItem, VerticalDimensionItem

__author__ = "d41"

logger = logging.getLogger("blend")
logger.setLevel(logging.DEBUG)


# TODO: put dimensions to different layer with different scale

projector = (20, 20, 10)  # cm


class Scheme(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.annotations = None

        self.pen_surface = QtGui.QPen(QtCore.Qt.red)
        self.pen_surface.setWidth(3)
        self.pen_projection = QtGui.QPen(QtCore.Qt.black)
        self.pen_projection.setWidth(1)
        self.pen_projector = QtGui.QPen(QtCore.Qt.black)
        self.pen_projector.setWidth(3)
        self.pen_illusion = QtGui.QPen(QtCore.Qt.black)
        self.pen_illusion.setWidth(1)
        self.pen_illusion.setStyle(QtCore.Qt.DashLine)
        self.pen_dimensions = QtGui.QPen(QtCore.Qt.black)
        self.pen_dimensions.setWidth(1)

    def drawHorizontalDimension(self, x1, y_base, x2, y_shift, label):
        dimension = HorizontalDimensionItem(x1, y_base, x2, y_shift, label)
        self.addItem(dimension)
        self.annotations.append(dimension)

    def drawVerticalDimension(self, x_base, y1, x_shift, y2, label):
        dimension = VerticalDimensionItem(x_base, y1, x_shift, y2, label)
        self.addItem(dimension)
        self.annotations.append(dimension)


class FrontScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.surface = None
        self.projections = None
        self.projectors = None
        self.annotations = None

        self.resetItems()

    def resetItems(self):
        self.surface = None
        self.projections = []
        self.projectors = []
        self.annotations = []

    def initializeItems(self, surface, projector_count, projection, side_step, main_step):
        self.clear()
        self.resetItems()

        # draw surface
        self.surface = self.addRect(0, 0, surface.width, surface.height, pen=self.pen_surface)
        self.drawHorizontalDimension(0, surface.height, surface.width, surface.height + 30,
                                     "{:0.1f} cm : {} px".format(surface.width, surface.res_x))
        self.drawVerticalDimension(0, 0, -30, surface.height,
                                   "{:0.1f} cm : {} px".format(surface.height, surface.res_y))

        # draw projections
        for num in range(projector_count):
            item = self.addRect(
                num * main_step, 0,
                projection.width, projection.height, pen=self.pen_projection)  # cm
            self.projections.append(item)

            item = self.addRect(
                (side_step + num * main_step) - projector[1] / 2,
                -(10.0 + projector[2]),
                projector[1], projector[2], pen=self.pen_projector)  # cm
            self.projectors.append(item)

            x, y = (side_step + num * main_step), -10
            item = self.addLine(x, y, (num * main_step), 0, pen=self.pen_illusion)  # cm
            self.annotations.append(item)
            item = self.addLine(x, y, (num * main_step), surface.height, pen=self.pen_illusion)  # cm
            self.annotations.append(item)
            item = self.addLine(x, y, x + side_step, 0, pen=self.pen_illusion)  # cm
            self.annotations.append(item)
            item = self.addLine(x, y, x + side_step, surface.height, pen=self.pen_illusion)  # cm
            self.annotations.append(item)

        self.drawHorizontalDimension(0, 0, projection.width, -30,
                                     "{:0.1f} cm : {} px".format(projection.width, projection.res_x))


class TopScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.surface = None
        self.projectors = None
        self.annotations = None

        self.resetItems()

    def resetItems(self):
        self.surface = None
        self.projectors = []
        self.annotations = []

    def initializeItems(self, surface, projector_count, side_step, main_step, distance):
        self.clear()
        self.resetItems()

        # draw surface
        self.surface = self.addLine(0, 0, surface.width, 0, pen=self.pen_surface)
        self.drawHorizontalDimension(0, 0, surface.width, -30, "{:0.1f} cm".format(surface.width))
        self.drawVerticalDimension(0, 0, -30, distance, "{:0.1f} cm".format(distance))

        # draw projections
        for num in range(projector_count):
            item = self.addRect(
                (side_step + num * main_step) - projector[0] / 2,
                distance,
                projector[0], projector[1], pen=self.pen_projector)
            self.projectors.append(item)

            x, y = side_step + num * main_step, distance
            item = self.addLine(x, y, num * main_step, 0, pen=self.pen_illusion)
            self.annotations.append(item)
            item = self.addLine(x, y, x + side_step, 0, pen=self.pen_illusion)
            self.annotations.append(item)

        # draw steps
        self.drawHorizontalDimension(0, distance, side_step, distance + 30, "{:0.1f} cm".format(side_step))

        if projector_count > 1:
            self.drawHorizontalDimension(side_step, distance, side_step + main_step, distance + 30,
                                         "{:0.1f} cm".format(main_step))


class LeftScheme(Scheme):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.surface = None
        self.projectors = None
        self.annotations = None

    def resetItems(self):
        self.surface = None
        self.projectors = None
        self.annotations = []

    def initializeItems(self, surface, distance):
        self.clear()
        self.resetItems()
